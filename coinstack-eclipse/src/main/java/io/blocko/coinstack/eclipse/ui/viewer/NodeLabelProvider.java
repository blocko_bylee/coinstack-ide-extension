package io.blocko.coinstack.eclipse.ui.viewer;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;

public class
NodeLabelProvider
extends LabelProvider {
    
    protected ISharedImages images;
    
    public
    NodeLabelProvider() {
    }
    
    public
    NodeLabelProvider(
        final ISharedImages images ) {
        this.images = images;
    }
    
    public
    void
    setSharedImages(
        final ISharedImages images ) {
        this.images = images;
    }
    

    public
    String
    getText(
        final Object obj) {
        return obj.toString();
    }
    public Image getImage(Object obj) {
        String imageKey = ISharedImages.IMG_OBJ_ELEMENT;
        if ( obj instanceof TreeParent ) {
            imageKey = ISharedImages.IMG_OBJ_FOLDER;
        }
        return images.getImage( imageKey );
    }
}
