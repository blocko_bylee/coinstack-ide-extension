//
// Note: This example test is leveraging the Mocha test framework.
// Please refer to their documentation on https://mochajs.org/ for help.
//

// The module 'assert' provides assertion methods from node
import * as assert from 'assert';

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import * as vscode from 'vscode';
import * as myExtension from '../../../src/extension';

import { CoinstackJsonParser } from '../../../src/model/document/parser';


// Defines a Mocha test suite to group tests of similar kind together
suite('Parser Tests', () => {
    test( 'Parsing coinstack.json', () => {
        const parser = new CoinstackJsonParser();
        const input =
        `{
            "endpoints": [ {
                "hostname": "localhost",
                "port": 4000,
                "privateKeys": []
            }, {
                "hostname": "localhost2",
                "port": 7000,
                "privateKeys": []
            } ],
            "test":"aaa"
        }`;
        parser.parse( new Buffer( input ) );
    } )
});