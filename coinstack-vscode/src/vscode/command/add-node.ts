import * as _ from 'lodash';
import * as vscode from 'vscode';

import { Node }                               from '../../model';
import { AbstractAction, Actions }            from '../../action';
import { Command }                            from '../command';

export class AddNodeCommand implements Command {
  id = 'extension.addNode';
  execute(...args: any[]):void {
    console.log( 'Request to add a node' );
    vscode.window.showInputBox( {
      prompt: '노드의 엔드포인트를 입력하세요. 엔드포인트는 호스트의 이름과 포트로 구성됩니다.',
      placeHolder: 'localhost:3338'
    } ).then( _.bind( this.onInputCompleted, this ) );
  }
  onInputCompleted( value: string ): void {
    if ( null == value ) {
      return ;
    }
    const node = Node.from( value );
    const action = new AddNodeAction( node );
    Actions.getManager().do( action );
  }
}

export class AddNodeAction extends AbstractAction {
  node: Node;

  constructor( node: Node ) {
    super();
    this.node = node;
  }

  do(): void {
    return null;
  }

}