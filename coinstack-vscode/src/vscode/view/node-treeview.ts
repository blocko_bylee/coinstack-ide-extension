import * as _ from 'lodash';
import * as fs from 'fs';
import * as path from 'path';
import * as vscode from 'vscode';
import * as json from 'jsonc-parser';

import { watch, unwatch } from '../../util';
import getLogger from '../../util/logger';
import { Parent, Child, Node, PrivateKey, PublicKey, Address } from '../../model';
import { CoinstackJsonParser } from '../../model/document/parser';
import { Context } from '../../constant';

const NODE_ITEM_CONTEXT_VALUE           = 'coinstack/node';
const PRIVATE_KEY_ITEM_CONTEXT_VALUE    = 'coinstack/privatekey';
const PUBLIC_KEY_ITEM_CONTEXT_VALUE     = 'coinstack/publickey';
const ADDRESS_ITEM_CONTEXT_VALUE        = 'coinstack/address';

class NodeItem extends vscode.TreeItem {
  endpoinst: Node;
  contextValue = NODE_ITEM_CONTEXT_VALUE;

  constructor( endpoint: Node ) {
    super(
      endpoint.toString(),
      (0==endpoint.privateKeys.length)?vscode.TreeItemCollapsibleState.None:vscode.TreeItemCollapsibleState.Collapsed
    );
    this.endpoinst = endpoint;
  }
}

class PrivateKeyItem extends vscode.TreeItem {
  contextValue = PRIVATE_KEY_ITEM_CONTEXT_VALUE;
  privateKey: PrivateKey;
  constructor( privateKey?: PrivateKey ) {
    super(
      privateKey.alias || privateKey.value,
      (null==privateKey.getPublicKey())?vscode.TreeItemCollapsibleState.Collapsed:vscode.TreeItemCollapsibleState.Collapsed
    );
    this.privateKey = privateKey;
  }
}
class PublicKeyItem extends vscode.TreeItem {
  contextValue = PUBLIC_KEY_ITEM_CONTEXT_VALUE;
  privateKey: PrivateKey;
  constructor( privateKey?: PrivateKey ) {
    super(
      privateKey.getPublicKey().toHexaString(),
      vscode.TreeItemCollapsibleState.None
    );
    this.privateKey = privateKey;
    this.collapsibleState = vscode.TreeItemCollapsibleState.None
  }
}

class AddressItem extends vscode.TreeItem {
  contextValue = ADDRESS_ITEM_CONTEXT_VALUE;
  privateKey: PrivateKey;
  constructor( privateKey?: PrivateKey ) {
    super( privateKey.getAddress().toHexaString(), vscode.TreeItemCollapsibleState.None  );
    this.privateKey = privateKey;
  }
}

export class CoinstackNodeProvider
implements vscode.TreeDataProvider<any> {
  logger = getLogger( 'coinstack.view.nodes' );
  private _onDidChangeTreeData: vscode.EventEmitter<json.Node | null> = new vscode.EventEmitter<json.Node | null>();
  readonly onDidChangeTreeData: vscode.Event<json.Node | null> = this._onDidChangeTreeData.event;
  input: Node[] = [];
  
  constructor() {
    watch( this, 'input', _.bind( this.notifyChange, this ) );
  }

  notifyChange( newVal, oldVal ) {
    this.logger.debug( 'notify tree change' );
    this._onDidChangeTreeData.fire();
    return newVal;
  }

  onDocumentChanged( changeEvent: vscode.TextDocumentChangeEvent ): void {
    this.logger.debug( 'Document changed', changeEvent );

    if ( null == vscode.workspace.rootPath ) {
      this.logger.info( 'No root! User not choose working folder yet.' );
    } else {
      this.logger.verbose( 'Target file: ' + Context.getCoinstackFilepath() );
      if ( null == changeEvent ) {
        fs.readFile( Context.getCoinstackFilepath(), _.bind( this.parse, this ) );
      } else {
        this.logger.verbose( 'Changed document file: ' + changeEvent.document.fileName );
        if ( changeEvent.document.fileName === Context.getCoinstackFilepath() ) {
          this.parse( null, new Buffer( changeEvent.document.getText() ) );
        }
      } 
    }
  }

  parse( err: NodeJS.ErrnoException, data: Buffer ): void {
    const parser = new CoinstackJsonParser();
    const rootNode = parser.parse( data );
    this.logger.debug( 'Root:', rootNode );
    this.input = rootNode;
    this.logger.info( 'Root changed' );
  }

  getCanonicalForm( element: Child ): any {
    return null;
  }
  
	/**
	 * Get [TreeItem](#TreeItem) representation of the `element`
	 *
	 * @param element The element for which [TreeItem](#TreeItem) representation is asked for.
	 * @return [TreeItem](#TreeItem) representation of the element
	 */
	getTreeItem( element: any ): vscode.TreeItem {
    console.log( 'Need tree item for', element );
    this.logger.verbose( 'Need tree item for', element );
    if ( element instanceof Node ) {
      return new NodeItem( element );
    } else if ( element instanceof PrivateKey ) {
      return new PrivateKeyItem( element );
    } else if ( element instanceof PublicKey ) {
      return new PublicKeyItem( element.getPrivateKey() );
    } else if ( element instanceof Address ) {
      return new AddressItem( element.getPrivateKey() );
    }
    throw new Error( 'Unknown type' );
  }

  getChildren( element?: any ): vscode.ProviderResult<any[]> {
    console.log( 'Children for ', element );
    this.logger.verbose( 'Children for ', element );
    if ( null == element ) {
      return this.input;
    } else if ( 'getChildren' in element ) {
      return element.getChildren();
    }
    return null;
  }
}