import * as _ from 'lodash';
import * as vscode from 'vscode';

import getLogger from '../util/logger';

import { CoinstackNodeProvider } from './view/node-treeview'

import { Command } from './command';
import { AddNodeCommand } from './command/add-node';

class CommandManager {
  logger = getLogger( 'vscode.extension' );
  register( command: Command ): void {
    this.logger.info( 'Register ', command );
    vscode.commands.registerCommand( command.id, command.execute )
  }
}

export class ExtensionManager {
  logger = getLogger( 'vscode.extension' );
  context: vscode.ExtensionContext;
  commands = [ new AddNodeCommand() ];
  commandManager = new CommandManager();

  activate(context: vscode.ExtensionContext) {
    if ( null != this.context ) {
      throw new Error( 'Extensions already activated!' );
    }
    this.context = context;
    this.logger.info( 'Coinstack extension activated.' );

    const nodePrivder = new CoinstackNodeProvider();
    nodePrivder.onDocumentChanged( null );
    vscode.window.registerTreeDataProvider( 'coinstack-nodes', nodePrivder );
    vscode.workspace.onDidChangeTextDocument( _.bind( nodePrivder.onDocumentChanged, nodePrivder ) );

    _.each( this.commands, _.bind( this.commandManager.register, this.commandManager ) );
  }

  deactivate() {
    if ( null == this.context ) {
      throw new Error( 'Extensions alread deactivated!' );
    }
    this.context = null;
    this.logger.info( 'Coinstack extension deativated.' );
  }
}


