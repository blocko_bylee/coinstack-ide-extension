import * as winston from 'winston';

const winstonConsole = new (winston.transports.Console)({ json: false, timestamp: true });

const loggers = {};
const opts = {
  level               : 'debug',
  transports          : [ winstonConsole ],
  exceptionHandlers   : [ winstonConsole ],
  exitOnError         : false
};
export default function( name: string ) {
  if ( loggers[name] ) {
    return loggers[name];
  }
  const logger = new winston.Logger( opts );
  logger.configure( opts );
  loggers[name] = logger;
  return logger;
}