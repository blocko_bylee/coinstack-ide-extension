import * as vscode from 'vscode';


export const Context = {
  DEFAULT_NODE_PORT: 8333,
  getCoinstackFilepath(): string {
    return vscode.workspace.rootPath + '/coinstack.json';
  }
};
