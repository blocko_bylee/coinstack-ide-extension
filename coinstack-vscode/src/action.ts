export interface Action {
  canDo(): boolean;
  do(): UndoAction|void;
}

export interface UndoAction {
  originAction: Action;
}

export class AbstractAction implements Action {
  canDo(): boolean {
    return true;
  }
  do(): UndoAction|void {
    return null;
  }
}

export class ActionManager {
  do( action: Action ) {
    if ( action.canDo() ) {
      action.do();
    }
  }
}

const actionManager = new ActionManager();

export class Actions {
  static getManager() {
    return actionManager;
  }
}
