
export function watch( o: Object, name: string, handler: Function) {
  var val = o[name];
  const getter = function () {
      return val;
  };
  const setter = function (newval) {
      return val = handler.call(handler, newval, val);
  };
  if (delete o[name]) { // can't watch constants
      if (Object.defineProperty) { // ECMAScript 5
          Object.defineProperty(o, name, {
              get: getter,
              set: setter
          } );
      }
  }
}

export function unwatch( o: Object, name: string ) {
  var val = o[name];
  delete o[name]; // remove accessors
  o[name] = val;
}