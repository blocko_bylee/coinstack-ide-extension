import * as _ from 'lodash';
import * as json from 'jsonc-parser';
import { Node, PrivateKey } from '../../model';
import getLogger from '../../util/logger';

export class CoinstackJsonParser {
  logger = getLogger( 'model.documenparser' )
  parse( data: Buffer ): Node[] {
    const str = data.toString();
    this.logger.verbose( 'Json', str );
    const rootNode: json.Node = json.parseTree( str );
    return this.visitRoot( rootNode );
  }
  visitRoot( node: json.Node ): Node[] {
    this.logger.debug( '[Root]' );
    if ( 'object' === node.type ) {
      if ( node.children ) {
        const r = _.flatten( _.filter( node.children, function( child ) {
          return 'property' === child.type && 'endpoints' === child.children[0].value;
        } )
        .map( function( child ) {
          return this.visitNodes( child.children[0].value, child.children[1] );
        }, this ) );
        this.logger.verbose( 'Extracted models:', r );
        return r;
      } else {
        this.logger.info( 'Empty object in root' );
      }
    } else {
      this.logger.warn( 'An object expected in root' );
    }
  }

  visitNodes( key: string, value: json.Node ): Node[] {
    const nodes: Node[] = [];
    if ( 'array' === value.type ) {
      for ( const child of value.children ) {
        const node = this.visitNode( child );
        node && nodes.push( node );
      }
    } else {
      this.logger.warn( 'endpoints must be array' );
    }
    return nodes;
  }

  visitNode( node: json.Node ): Node {
    if ( 'object' === node.type ) {
      const endpoint = new Node();
      for ( const child of node.children ) {
        this.visitEndpointProperty( endpoint, child.children[0].value, child.children[1] );
      }
      return endpoint;
    } else {
      this.logger.warn( 'endpoint expected' );
    }
    this.logger.debug( 'Endpoint', node );
    return null;
  }

  visitEndpointProperty( endpoint: Node, key: string, value: json.Node ) {
    if ( 'hostname' === key ) {
      if ( 'string' === value.type ) {
        endpoint.hostname = value.value;
      } else {
        this.logger.warn( 'Hostname must be string type' );
      }

    } else if ( 'port' === key ) {
      if ( 'number' === value.type ) {
        endpoint.port = value.value;
      } else {
        this.logger.warn( 'Hostname must be number type' );
      }

    } else if ( 'privateKeys' === key ) {
      if ( 'array' === value.type ) {
        for ( const child of value.children ) {
          const key = this.visitPrivateKey( child );
          key && ( key.node = endpoint, endpoint.privateKeys.push( key ) );

        }
      } else {
        this.logger.warn( 'Hostname must be array type' );
      }
    } else {
      this.logger.error( 'Unknown property: ' + key );
    }
  }

  visitPrivateKey( key: json.Node ): PrivateKey {
    if ( 'object' === key.type ) {
      const privateKey = new PrivateKey( null, null );
      for ( const child of key.children ) {
        this.visitPrivateKeyProperty( privateKey, child.children[0].value, child.children[1] );
      }
      return privateKey;
    } else {
      this.logger.warn( 'Key must be string' );
      return null;
    }
  }

  visitPrivateKeyProperty( privateKey: PrivateKey, key: string, value: json.Node ) {
    if ( 'string' === value.type ) {
      if ( 'alias' === key ) {
        privateKey.alias = value.value;
      } else if ( 'value' === key ) {
        privateKey.value = value.value;
      }
    }
  }
}