import * as _             from 'lodash';
import * as coinstack     from 'coinstack-sdk-js';

import { Context }        from './constant';

export interface Child {
  getParent(): any;
}

export interface Parent {
  getChildren(): any[];
}

export class Node implements Parent {
  hostname: string;
  port: number;
  privateKeys: PrivateKey[] = [];
  static from( endpoint: string ): Node {
    const safe = endpoint.trim();
    const index = safe.indexOf( ':' );
    const node = new Node();
    if ( index < 0 ) {
      node.hostname = safe;
      node.port = Context.DEFAULT_NODE_PORT;
    } else {
      node.hostname = safe.substring( 0, index );
      node.port = parseInt( safe.substring( index + 1 ) );
    }
    return null;
  }
  getChildren(): any[] {
    return this.privateKeys;
  }
  toString(): string {
    if ( this.port && 0  < this.port ) {
      return (this.hostname || '') + ':' + this.port;
    } else {
      return this.hostname || '';
    }
  }
}

interface HexaString {
  value: string;
  toHexaString(): string;
}

export class PrivateKey implements Parent, Child, HexaString {
  alias?: string;
  node: Node;
  value: string;
  constructor( node: Node, value: string, alias?: string ) {
    this.node = node;
    this.value = value;
    this.alias = alias;
  }
  getParent(): any {
    return this.node;
  }
  getChildren(): any[] {
    return [ this.getPublicKey(), this.getAddress() ];
  }
  
  getAddress(): Address {
    return this.getPublicKey().getAddress();
  }
  getPublicKey(): PublicKey {
    return new PublicKey( this, coinstack.ECKey.derivePubKey( this.value ) );
  }
  toHexaString(): string {
    return this.value;
  }
}

export class PublicKey implements Child, HexaString {
  value: string;
  privateKey: PrivateKey;
  constructor( privateKey: PrivateKey, value: string ) {
    this.privateKey = privateKey;
    this.value = value;
  }

  getParent(): any {
    return this.privateKey;
  }

  getAddress(): Address {
    if ( this.privateKey ) {
      return new Address( this, coinstack.ECKey.deriveAddress( this.privateKey.value ) );
    } else {
      return null;
    }
  }

  getPrivateKey(): PrivateKey {
    return this.privateKey;
  }

  toHexaString(): string {
    return this.value;
  }
}

export class Address implements HexaString {
  value: string;
  publicKey: PublicKey;
  constructor( publicKey: PublicKey, value: string ) {
    this.publicKey = publicKey;
    this.value = value;
  }

  getPublicKey(): PublicKey {
    return this.publicKey;
  }

  getPrivateKey(): PrivateKey {
    return this.publicKey.getPrivateKey();
  }
  toHexaString(): string {
    return this.value;
  }

}




